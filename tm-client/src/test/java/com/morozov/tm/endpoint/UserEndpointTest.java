package com.morozov.tm.endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserEndpointTest {
    private SessionEndpoint sessionEndpoint;
    private UserEndpoint userEndpoint;

    @Before
    public void setUp() throws Exception {
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        userEndpoint = new UserEndpointService().getUserEndpointPort();
    }

    @Test
    public void findOneUserByIdTest() throws UserNotFoundException_Exception, CloneNotSupportedException_Exception, AccessFirbidenException_Exception {
        final String login = "test";
        final String password = "test";
        SessionDto session = sessionEndpoint.openSession(login, password);
        UserDto userDto = userEndpoint.findOneUserById(session);
        Assert.assertNotNull(userDto);
    }

    @Test(expected = AccessFirbidenException_Exception.class)
    public void accessFirbidenExceptionTest() throws AccessFirbidenException_Exception, UserNotFoundException_Exception, CloneNotSupportedException_Exception {
        SessionDto fakeSession = new SessionDto();
        userEndpoint.findOneUserById(fakeSession);
    }


}