package com.morozov.tm.endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SessionEndpointTest {

    private SessionEndpoint sessionEndpoint;

    @Before
    public void setUp() throws Exception {
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
    }

    @Test
    public void openSessionTest() throws UserNotFoundException_Exception {
        final String login = "test";
        final String password = "test";
        SessionDto session = sessionEndpoint.openSession(login,password);
        Assert.assertNotNull(session);
    }
    @Test(expected = UserNotFoundException_Exception.class)
    public void userNotFoundTest() throws UserNotFoundException_Exception {
        final String login = "user";
        final String password = "sdldlkfmv";
        SessionDto session = sessionEndpoint.openSession(login,password);
    }

}