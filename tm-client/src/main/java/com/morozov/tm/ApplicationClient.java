package com.morozov.tm;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.configuration.EndpointConfigurationBean;
import com.morozov.tm.service.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Set;

public class ApplicationClient {
    private static final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("com.morozov.tm").getSubTypesOf(AbstractCommand.class);
    public static void main(String[] args) {
        @NotNull ApplicationContext context = new AnnotationConfigApplicationContext(EndpointConfigurationBean.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init(classes);
    }
}
