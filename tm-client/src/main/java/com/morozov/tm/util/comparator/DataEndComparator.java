package com.morozov.tm.util.comparator;

import com.morozov.tm.endpoint.AbstractWorkEntity;
import com.morozov.tm.endpoint.AbstractWorkEntityDto;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class DataEndComparator implements Comparator<AbstractWorkEntityDto> {
    @Override
    public int compare(@NotNull final AbstractWorkEntityDto o1, @NotNull final AbstractWorkEntityDto o2) {
        if (o1.getEndDate() == null && o2.getEndDate() == null) return 0;
        if (o1.getEndDate() != null && o2.getEndDate() == null) return -1;
        if (o1.getEndDate() == null && o2.getEndDate() != null) return 1;
        return o1.getEndDate().compare(o2.getEndDate());
    }
}
