package com.morozov.tm.util.comparator;


import com.morozov.tm.endpoint.AbstractWorkEntity;
import com.morozov.tm.endpoint.AbstractWorkEntityDto;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class DataCreateComparator implements Comparator<AbstractWorkEntityDto> {
    @Override
    public int compare(@NotNull final AbstractWorkEntityDto o1, @NotNull final AbstractWorkEntityDto o2) {
        return o1.getCreatedData().compare(o2.getCreatedData());
    }
}
