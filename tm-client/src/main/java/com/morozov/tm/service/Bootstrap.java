package com.morozov.tm.service;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.CommandCorruptException;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public final class Bootstrap implements IServiceLocator {
    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();
    @Autowired
    ApplicationContext context;

    @Nullable
    private SessionDto session = null;

    public void init(@NotNull Set<Class<? extends AbstractCommand>> classes) {
        ConsoleHelperUtil.writeString("Вас приветствует программа Task Manager. " +
                "Наберите \"help\" для вывода списка доступных команд. ");
        @NotNull String console = "";
        while (!"exit".equals(console)) {
            commands.clear();
            try {
                fillCommands(classes, context);
                console = ConsoleHelperUtil.readString();
                AbstractCommand command = commands.get(console);
                if (command != null) {
                    command.execute();
                }
            } catch (Exception e) {
                ConsoleHelperUtil.writeString(e.getMessage());
            }
        }
    }

    @Override
    public @Nullable SessionDto getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable SessionDto session) {
        this.session = session;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commands.values());
    }

    private void registry(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @Nullable final String cliCommand = command.getName();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    private void fillCommands(@NotNull Set<Class<? extends AbstractCommand>> classes, ApplicationContext context)
            throws CommandCorruptException {
        for (Class clazz : classes) {
            @NotNull AbstractCommand command = (AbstractCommand)context.getBean(clazz);
            registryAllUserCommand(command);
            if (session != null) {
                registryUserSessionCommand(command);
            } else {
                registryEmptyUserSessionCommand(command);
            }
        }
    }

    private void registryAllUserCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        if (command.getUserRoleList().contains(UserRoleEnum.ALL)) registry(command);
    }

    private void registryEmptyUserSessionCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        if (command.getUserRoleList().isEmpty()) registry(command);
    }

    private void registryUserSessionCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        if (session == null) return;
        if (session.getUserRoleEnum().value().equals("ADMIN")) {
            if (command.getUserRoleList().contains(UserRoleEnum.ADMIN)) {
                registry(command);
            }
        }
        if (session.getUserRoleEnum().value().equals("USER")) {
            if (command.getUserRoleList().contains(UserRoleEnum.USER)) {
                registry(command);
            }
        }
    }
}

