package com.morozov.tm.command;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public abstract class AbstractCommand {
    @Autowired
    private UserEndpoint userEndpoint;
    @Autowired
    private DomainEndpoint domainEndpoint;
    @Autowired
    private TaskEndpoint taskEndpoint;
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private SessionEndpoint sessionEndpoint;
    @Autowired
    private IServiceLocator serviceLocator;

    protected List<UserRoleEnum> userRoleList = new ArrayList<>();


    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException, ClassNotFoundException, JAXBException, UserNotFoundException_Exception,
            StringEmptyException_Exception, UserExistException_Exception, AccessFirbidenException_Exception,
            CloneNotSupportedException_Exception, RepositoryEmptyException_Exception, ParseException_Exception,
            TaskNotFoundException_Exception, ProjectNotFoundException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception;

    public List<UserRoleEnum> getUserRoleList() {
        return userRoleList;
    }
}
