package com.morozov.tm.command;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserUpdateProfileCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private UserEndpoint userEndpoint;

    public UserUpdateProfileCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-update-profile";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update user profile";
    }

    @Override
    final public void execute() throws CloneNotSupportedException_Exception, UserNotFoundException_Exception,
            AccessFirbidenException_Exception, StringEmptyException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите новое имя пользователя");
        @NotNull final String newUserName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новый пароль");
        @NotNull final String newUserPassword = ConsoleHelperUtil.readString();
        if(newUserName.isEmpty() || newUserPassword.isEmpty()) throw  new StringEmptyException_Exception();
        userEndpoint.updateUserProfile(session, newUserName, newUserPassword);
        ConsoleHelperUtil.writeString("Профиль обновлен");
    }
}
