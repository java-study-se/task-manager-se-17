package com.morozov.tm.command.system;


import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelpCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;

    public HelpCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ALL);
    }

    @NotNull
    @Override
    final public String getName() {
        return "help";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show all command";
    }

    @Override
    final public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommandList()) {
            ConsoleHelperUtil.writeString(String.format("%s: %s", command.getName(), command.getDescription()));
        }
    }
}
