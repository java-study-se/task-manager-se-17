package com.morozov.tm.command.data.json;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@Component
public class DataJsonJaxBLoadCommand extends AbstractCommand {
    @Autowired
    private IServiceLocator serviceLocator;
    @Autowired
    private DomainEndpoint domainEndpoint;

    public DataJsonJaxBLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-json-jaxb";
    }

    @Override
    public String getDescription() {
        return "Load data from json file with Jax-B library";
    }

    @Override
    public void execute() throws FileNotFoundException, JAXBException, CloneNotSupportedException_Exception,
            AccessFirbidenException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception,
            UserNotFoundException_Exception {
        final @Nullable SessionDto session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_JSON.getPath());
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_JSON.getPath());
        @NotNull final File sourceFile = pathToFile.toFile();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{Domain.class}, properties);
        if (jaxbContext == null) return;
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        if (unmarshaller == null) return;
        final DomainDto domain = (DomainDto) unmarshaller.unmarshal(sourceFile);
        if (domain == null) return;
        domainEndpoint.load(session, domain);
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
