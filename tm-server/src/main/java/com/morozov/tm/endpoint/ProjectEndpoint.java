package com.morozov.tm.endpoint;

import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.api.service.ISessionService;
import com.morozov.tm.api.service.IUserService;
import com.morozov.tm.dto.ProjectDto;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@NoArgsConstructor
@WebService
@Component
public class ProjectEndpoint {
    @Autowired
    private IProjectService projectService;
    @Autowired
    private ISessionService sessionService;
    @Autowired
    private IUserService userService;

    public List<ProjectDto> findAllProject(
            @NotNull @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        List<Project> allProject = projectService.findAllProject();
        return projectService.transferListProjectToListProjectDto(allProject);
    }

    @WebMethod
    public List<ProjectDto> findAllProjectByUserId(
            @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        List<Project> allProjectByUserId = projectService.getAllProjectByUserId(sessionUser.getId());
        return projectService.transferListProjectToListProjectDto(allProjectByUserId);
    }

    @WebMethod
    public ProjectDto addProject(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "projectName") String projectName)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        Project project = projectService.addProject(projectName, sessionUser);
        return projectService.transferProjectToProjectDto(project);
    }

    @WebMethod
    public boolean removeProjectById(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "projectId") String id)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, UserNotFoundException,
            ProjectNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        return projectService.removeProjectById(sessionUser.getId(), id);
    }

    @WebMethod
    public void updateProject(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "projectId") String id,
            @NotNull @WebParam(name = "projectName") String projectName,
            @NotNull @WebParam(name = "projectDescription") String projectDescription,
            @NotNull @WebParam(name = "dataStartProject") String dataStart,
            @NotNull @WebParam(name = "dataEndProject") String dataEnd)
            throws ParseException, ProjectNotFoundException, CloneNotSupportedException, AccessFirbidenException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        projectService.updateProject(id, projectName, projectDescription, dataStart, dataEnd, sessionUser);
    }

    @WebMethod
    public List<ProjectDto> findProjectByStringInNameOrDescription(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "string") String string)
            throws CloneNotSupportedException, AccessFirbidenException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        List<Project> projectBySearchString = projectService.searchProjectByString(sessionUser.getId(), string);
        return projectService.transferListProjectToListProjectDto(projectBySearchString);
    }

    @WebMethod
    public void removeAllProjectByUserId(
            @WebParam(name = "userId") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        projectService.removeAllByUserId(sessionUser.getId());
    }

    @WebMethod
    public void clearProjectList(
            @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        projectService.clearProjectList();
    }
}
