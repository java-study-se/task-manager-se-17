package com.morozov.tm.service;

import com.morozov.tm.endpoint.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.Endpoint;

@Component
public final class Bootstrap {
    private static final String URL = "http://localhost:";
    @Autowired
    private DomainEndpoint domainEndpoint;
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private SessionEndpoint sessionEndpoint;
    @Autowired
    private TaskEndpoint taskEndpoint;
    @Autowired
    private UserEndpoint userEndpoint;

    public void init() {
        @NotNull StringBuilder port = new StringBuilder("8080/");
        if (System.getProperty("server.port") != null) {
            port = new StringBuilder(System.getProperty("server.port")).append("/");
        }
        ConsoleHelperUtil.writeString("Публикация Endpoint:");
        @NotNull final String linkUrlTaskEndpoint = String.format("%s%s%s?wsdl", URL, port, taskEndpoint.getClass().getSimpleName());
        Endpoint.publish(linkUrlTaskEndpoint, taskEndpoint);
        ConsoleHelperUtil.writeString(String.format("Опубликован %s: %s", taskEndpoint.getClass().getSimpleName(), linkUrlTaskEndpoint));

        @NotNull final String linkUrlProjectEndpoint = String.format("%s%s%s?wsdl", URL, port, projectEndpoint.getClass().getSimpleName());
        Endpoint.publish(linkUrlProjectEndpoint, projectEndpoint);
        ConsoleHelperUtil.writeString(String.format("Опубликован %s: %s", projectEndpoint.getClass().getSimpleName(), linkUrlProjectEndpoint));

        @NotNull final String linkUrlUserEndPoint = String.format("%s%s%s?wsdl", URL, port, userEndpoint.getClass().getSimpleName());
        Endpoint.publish(linkUrlUserEndPoint, userEndpoint);
        ConsoleHelperUtil.writeString(String.format("Опубликован %s: %s", userEndpoint.getClass().getSimpleName(), linkUrlUserEndPoint));

        @NotNull final String linkUrlDomainEndpoint = String.format("%s%s%s?wsdl", URL, port, domainEndpoint.getClass().getSimpleName());
        Endpoint.publish(linkUrlDomainEndpoint, domainEndpoint);
        ConsoleHelperUtil.writeString(String.format("Опубликован %s: %s", domainEndpoint.getClass().getSimpleName(), linkUrlDomainEndpoint));

        @NotNull final String linkUrlSessionEndpoint = String.format("%s%s%s?wsdl", URL, port, sessionEndpoint.getClass().getSimpleName());
        Endpoint.publish(linkUrlSessionEndpoint, sessionEndpoint);
        ConsoleHelperUtil.writeString(String.format("Опубликован %s: %s", sessionEndpoint.getClass().getSimpleName(), linkUrlSessionEndpoint));
    }
}

