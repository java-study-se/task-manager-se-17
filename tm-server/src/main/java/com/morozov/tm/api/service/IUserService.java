package com.morozov.tm.api.service;

import com.morozov.tm.dto.UserDto;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;


import java.util.List;

public interface IUserService {
    User loginUser(@NotNull String login, @NotNull String password) throws UserNotFoundException;

    String registryUser(@NotNull String login, @NotNull String password) throws UserExistException, StringEmptyException;

    void updateUserPassword(@NotNull String id, @NotNull String newPassword) throws UserNotFoundException;

    void updateUserProfile(@NotNull String id, @NotNull String newUserName, @NotNull String newUserPassword)
            throws UserNotFoundException;

    void loadUserList(final List<User> userList);

    List<User> findAllUser();

    User findOneById(@NotNull final String id) throws UserNotFoundException;

    UserDto transferUserToUserDto(@NotNull final User user);

    List<UserDto> transferListUserToListUserDto(@NotNull final List<User> userList);
}
