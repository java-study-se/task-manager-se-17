package com.morozov.tm.api.repository;


import com.morozov.tm.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISessionRepository extends JpaRepository<Session,String> {

}
