package com.morozov.tm.api.service;

import com.morozov.tm.dto.DomainDto;
import org.jetbrains.annotations.NotNull;

public interface IDomainService {
    void load(@NotNull DomainDto domainDto);
    void export(@NotNull DomainDto domainDto);
}
