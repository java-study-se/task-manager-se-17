package com.morozov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.morozov.tm.enumerated.StatusEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@MappedSuperclass
public abstract class AbstractWorkEntity extends AbstractEntity {
    @NotNull
    @Column(nullable = false)
    private String name = "";
    @NotNull
    @Column(name = "date_create", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date createdData = new Date();
    @Nullable
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
    @NotNull
    @Column(nullable = false)
    private String description = "";
    @Nullable
    @Column(name = "date_start")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date startDate;
    @Nullable
    @Column(name = "date_end")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date endDate;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private StatusEnum status = StatusEnum.PLANNED;
}
