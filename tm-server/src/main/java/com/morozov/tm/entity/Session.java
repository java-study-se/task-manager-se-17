package com.morozov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractEntity implements Cloneable {
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
    @Nullable
    private String signature;
    @Nullable
    private Date timestamp;

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }
}
